# WayndowMaker
WayndowMaker is an attempt to re-implement [WindowMaker](https://www.windowmaker.org/) as a Wayland compositor. The name is just a working title; all the texts will still call it "Window Maker".

## Build
### CMake
Create a directory where you want to build, call `cmake` with `Ninja` as code generator (and your top level source directory as second parameter), and finally call `ninja`:

    mkdir build
    cd build
    cmake -G Ninja ..
    ninja

### autotools
Where is all the `autotools` code?

To be honest: My autotools fu is just enough to read and (mostly) understand what all the `*.ac` and `*.m4` files do, but I am just not able to modify it to make it work with *Wayland*.

If someone wants to re-add it: patches are welcome!